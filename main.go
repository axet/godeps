package main

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"os"
	"os/exec"
	"path"
	"strings"
)

type RunErr struct {
	error

	Cmd string
	Out string
}

func (m *RunErr) Error() string {
	if m == nil {
		return fmt.Sprintf("# m nil")
	}
	return fmt.Sprintf("# %s\n%s\n%s", m.Cmd, m.Out, m.error.Error())
}

func Split(ss string, sep string) (string, string) {
	at := strings.Split(ss, sep)
	if len(at) == 1 {
		return at[0], ""
	}

	return at[0], strings.Join(at[1:], sep)
}

func Exists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return true
}

func Run(args ...string) error {
	_, err := RunOut(args...)
	return err
}

func RunOut(args ...string) ([]byte, error) {
	return RunPwdOut("", args...)
}

func RunPwd(pwd string, args ...string) error {
	_, err := RunPwdOut(pwd, args...)
	return err
}

func RunPwdOut(pwd string, args ...string) ([]byte, error) {
	head := args[0]
	tail := args[1:]

	cmd := exec.Command(head, tail...)
	cmd.Dir = pwd

	out, err := cmd.CombinedOutput()

	if err != nil {
		return nil, &RunErr{err, strings.Join(args, " "), string(out)}
	}

	return out, nil
}

func Shell(s string) error {
	_, err := ShellOut(s)
	return err
}

func ShellOut(s string) ([]byte, error) {
	out, err := RunOut("sh", "-c", s)
	if err != nil {
		rerr := err.(*RunErr)
		return nil, &RunErr{rerr.error, s, rerr.Out}
	}
	return out, nil
}

func ParseDirs(dd ...string) ([]string, error) {
	var imports []string
	for _, d := range dd {
		ii, err := ParseDir(d)
		if err != nil {
			return nil, err
		}
		imports = append(imports, ii...)
	}
	return imports, nil
}

func ParseDir(dir string) ([]string, error) {
	var imports []string

	dirFile, err := os.Open(dir)
	if err != nil {
		return nil, err
	}
	defer dirFile.Close()
	info, err := dirFile.Stat()
	if err != nil {
		return nil, err
	}

	var pkgs map[string]*ast.Package

	if !info.IsDir() {
		var p *ast.File
		p, err = parser.ParseFile(token.NewFileSet(), dir, nil, 0)
		var pp ast.Package
		pkgs = make(map[string]*ast.Package)
		pp.Files = make(map[string]*ast.File)
		pp.Files[dir] = p
		pkgs[dir] = &pp
	} else {
		pkgs, err = parser.ParseDir(token.NewFileSet(), dir, FilterGo, 0)
		if err != nil {
			return nil, err
		}
	}

	for _, pkg := range pkgs {
		// var f *ast.File
		for _, f := range pkg.Files {
			for _, i := range f.Imports {
				d := strings.Trim(i.Path.Value, "\"")
				// from go/src/cmd/go/pkg.go
				//
				// p.Standard = p.Goroot && p.ImportPath != "" && !strings.Contains(p.ImportPath, ".")
				//
				// all Standard packages do not have "dot"
				if strings.Contains(d, ".") && !strings.HasPrefix(d, ".") {
					imports = append(imports, d)
				}
			}
		}
	}

	{
		dirs, err := dirFile.Readdir(-1)
		if err != nil {
			return nil, err
		}
		for _, info := range dirs {
			if info.IsDir() {
				t, e := ParseDir(path.Join(dir, info.Name()))
				if e != nil {
					return nil, e
				}
				imports = append(imports, t...)
			}
		}
	}

	return imports, nil
}

func FilterGo(info os.FileInfo) bool {
	name := info.Name()
	return !info.IsDir() && path.Ext(name) == ".go" && !strings.HasSuffix(name, "_test.go")
}

// update dest repo
func Update(dest string) (bool, error) {
	out1, err := RunPwdOut(dest, "git", "rev-parse", "HEAD")
	if err != nil {
		return false, err
	}
	err = RunPwd(dest, "git", "pull", "--rebase")
	if err != nil {
		return false, err
	}
	out2, err := RunPwdOut(dest, "git", "rev-parse", "HEAD")
	if err != nil {
		return false, err
	}
	if bytes.Compare(out1, out2) != 0 {
		return true, nil
	}
	return false, nil
}

func GoGet(d string) error {
	// ex: github.com/axet/test@123/222

	// github.com/axet/test, 123/222
	url, bp := Split(d, "@")
	// 123, 222
	branch, _ := Split(bp, "/")

	dest := os.ExpandEnv("$GOPATH/src/") + url
	if len(branch) != 0 {
		dest += "@" + branch
	}

	if !Exists(dest) {
		fmt.Println("- " + url)
		if len(branch) == 0 {
			err := Run("go", "get", url)
			if err != nil {
				return err
			}
		} else {
			err := Run("git", "clone", "https://"+url, "-b", branch, dest)
			if err != nil {
				return err
			}
		}
	} else {
		fmt.Println("- " + url)
		if len(branch) == 0 {
			err := Run("go", "get", "-u", url)
			if err != nil {
				return err
			}
		} else {
			err := RunPwd(dest, "git", "checkout", branch)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func main() {
	args := []string{"."}
	if len(os.Args) > 1 {
		args = os.Args[1:]
	}

	imports, err := ParseDirs(args...)
	if err != nil {
		fmt.Println(err)
		return
	}

	set := make(map[string]bool)
	for _, imp := range imports {
		set[imp] = true
	}
	imports = nil
	for key, _ := range set {
		imports = append(imports, key)
	}

	for _, d := range imports {
		err = GoGet(d)
		if err != nil {
			fmt.Println(err)
		}
	}
}
