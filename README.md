# godeps

Get Go dependencies automatically from a source files.

# install

```
# go get github.com/axet/godeps

# godeps

or

# GOPATH=$PWD godeps
```

# examples

`godeps` supports classic import section, and with tag/branch symantics.

```go
package main

import "github.com/axet/protocols/go"
```

Run `godeps` to get missing source.

Or you can specify a tag or branch name in go source import section. This technique does not break current go build system, go compiller finds sources just normally after you call `godeps` once.

```go
package main

import "github.com/axet/protocols@master/go"
```

Where `master` - is a branch name and `/go` - repo path

```go
package main

import "github.com/axet/protocols@protocols-0.0.7/go"
```

Where `protocols-0.0.7` - is a branch name and `/go` - repo path

  * https://github.com/axet/protocols
